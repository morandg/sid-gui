/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTestExt/MockSupport.h>

#include "SIDRegistersSinkMock.h"

namespace sid_synth::tests {

int SIDRegistersSinkMock::open() {
  return mock().actualCall(__func__).onObject(this).
      returnIntValue();
}

void SIDRegistersSinkMock::close() {
  mock().actualCall(__func__).onObject(this);
}

int SIDRegistersSinkMock::writeSIDRegister(uint8_t reg, uint8_t value) {
  return mock().actualCall(__func__).onObject(this).
      withParameter("reg", reg).
      withParameter("value", value).
      returnIntValue();
}

}       // namespace
