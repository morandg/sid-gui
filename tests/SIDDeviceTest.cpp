/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "mocks/SIDRegistersSinkMock.h"
#include "sid/SIDDevice.h"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace sid_synth;
using namespace sid_synth::tests;

TEST_GROUP(SIDDevice) {
  TEST_SETUP() {
  }

  TEST_TEARDOWN() {
    mock().clear();
  }

  std::unique_ptr<SIDDevice> BuildSIDDevice() {
    _sid_registers_sink = new SIDRegistersSinkMock();
    std::unique_ptr<ISIDRegistersSink> registers_sink(_sid_registers_sink);
    return std::make_unique<SIDDevice>(std::move(registers_sink));
  }

    SIDRegistersSinkMock* SIDRegistersSink() {
    return _sid_registers_sink;
  }

private:
    SIDRegistersSinkMock* _sid_registers_sink;
};

TEST(SIDDevice, setVolumeAfterFilter) {
  uint8_t volume = 0x0F;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x18).
      withParameter("value", 0x10).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x18).
      withParameter("value", 0x1f).
      andReturnValue(0);

  sid_device->enableFilter(SIDFilter::LowPass, true);
  sid_device->setVolume(volume);

  mock().checkExpectations();
}

TEST(SIDDevice, enableDisableFilterVoice3Off) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x18).
      withParameter("value", 0x80).
      andReturnValue(0);

  sid_device->enableFilter(SIDFilter::Voice3Off, true);

  mock().checkExpectations();
}

TEST(SIDDevice, enableDisableFilterHighPass) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x18).
      withParameter("value", 0x40).
      andReturnValue(0);

  sid_device->enableFilter(SIDFilter::HighPass, true);

  mock().checkExpectations();
}

TEST(SIDDevice, enableDisableFilterBandPass) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x18).
      withParameter("value", 0x20).
      andReturnValue(0);

  sid_device->enableFilter(SIDFilter::BandPass, true);

  mock().checkExpectations();
}

TEST(SIDDevice, enableDisableFilterLowPass) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x18).
      withParameter("value", 0x10).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x18).
      withParameter("value", 0x00).
      andReturnValue(0);

  sid_device->enableFilter(SIDFilter::LowPass, true);
  sid_device->enableFilter(SIDFilter::LowPass, false);

  mock().checkExpectations();
}

TEST(SIDDevice, enableFilterLowPassWriteFails) {
  int returned_code;
  int write_error = -25;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x18).
      withParameter("value", 0x10).
      andReturnValue(write_error);

  returned_code = sid_device->enableFilter(SIDFilter::LowPass, true);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_code);
}

TEST(SIDDevice, enableFilterLowPass) {
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x18).
      withParameter("value", 0x10).
      andReturnValue(0);

  returned_code = sid_device->enableFilter(SIDFilter::LowPass, true);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, enableDisableFilterVoice3) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x17).
      withParameter("value", 0x04).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x17).
      withParameter("value", 0x00).
      andReturnValue(0);

  sid_device->voice(SIDVoice::Voice3).enableFilter(true);
  sid_device->voice(SIDVoice::Voice3).enableFilter(false);

  mock().checkExpectations();
}

TEST(SIDDevice, enableFilterVoice3) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x17).
      withParameter("value", 0x04).
      andReturnValue(0);

  sid_device->voice(SIDVoice::Voice3).enableFilter(true);

  mock().checkExpectations();
}

TEST(SIDDevice, enableFilterVoice2) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x17).
      withParameter("value", 0x02).
      andReturnValue(0);

  sid_device->voice(SIDVoice::Voice2).enableFilter(true);

  mock().checkExpectations();
}

TEST(SIDDevice, enableFilterVoice1WriteFails) {
  int returned_code;
  int write_error = -64;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x17).
      withParameter("value", 0x01).
      andReturnValue(write_error);

  returned_code = sid_device->voice(SIDVoice::Voice1).enableFilter(true);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_code);
}

TEST(SIDDevice, enableFilterVoice1) {
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x17).
      withParameter("value", 0x01).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice1).enableFilter(true);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, setResonanceWriteFails) {
  int returned_code;
  int write_error = -23;
  uint8_t resonance = 0x0B;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x17).
      withParameter("value", 0xB0).
      andReturnValue(write_error);

  returned_code = sid_device->setResonance(resonance);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_code);
}

TEST(SIDDevice, setResonance) {
  int returned_code;
  uint8_t resonance = 0x0B;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x17).
      withParameter("value", 0xB0).
      andReturnValue(0);

  returned_code = sid_device->setResonance(resonance);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, setCutoffFrequencyWriteHighFails) {
  int returned_code;
  int write_error = -234;
  SIDCutOffFrequency cut_off_frequency = 0xFFFF;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x15).
      withParameter("value", 0x07).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x16).
      withParameter("value", 0xFF).
      andReturnValue(write_error);

  returned_code = sid_device->setCutOffFrequency(cut_off_frequency);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_code);
}

TEST(SIDDevice, setCutoffFrequencyWriteLowFails) {
  int returned_code;
  int write_error = -234;
  SIDCutOffFrequency cut_off_frequency = 0xFFFF;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x15).
      withParameter("value", 0x07).
      andReturnValue(write_error);

  returned_code = sid_device->setCutOffFrequency(cut_off_frequency);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_code);
}

TEST(SIDDevice, setCutoffFrequency) {
  int returned_code;
  SIDCutOffFrequency cut_off_frequency = 0xFFFF;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x15).
      withParameter("value", 0x07).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x16).
      withParameter("value", 0xFF).
      andReturnValue(0);

  returned_code = sid_device->setCutOffFrequency(cut_off_frequency);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, enableSyncVoice2) {
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0B).
      withParameter("value", 0x02).
      andReturnValue(0);


  returned_code = sid_device->voice(SIDVoice::Voice2).enableSync(true);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, enableSyncVoice1WriteFails) {
  int returned_code;
  int write_error = -32;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value", 0x02).
      andReturnValue(write_error);

  returned_code = sid_device->voice(SIDVoice::Voice1).enableSync(true);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_code);
}

TEST(SIDDevice, enableSyncVoice1) {
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value", 0x02).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice1).enableSync(true);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, enableRingVoice2) {
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0B).
      withParameter("value", 0x04).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice2).enableRing(true);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, enableRingVoice1WriteFails) {
  int returned_code;
  int write_error = -32;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value", 0x04).
      andReturnValue(write_error);

  returned_code = sid_device->voice(SIDVoice::Voice1).enableRing(true);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_code);
}

TEST(SIDDevice, enableRingVoice1) {
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value", 0x04).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice1).enableRing(true);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, setPulsWidthVoice2) {
  uint16_t pulse_width = 0x0123;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x09).
      withParameter("value",0x23).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0A).
      withParameter("value",0x01).
      andReturnValue(0);

  sid_device->voice(SIDVoice::Voice2).setPulseWidth(pulse_width);

  mock().checkExpectations();
}

TEST(SIDDevice, setPulsWidthWriteHighFails) {
  uint16_t pulse_width = 0x0ABC;
  int write_error = -44;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x02).
      withParameter("value",0xBC).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x03).
      withParameter("value",0x0A).
      andReturnValue(write_error);

  returned_code = sid_device->voice(SIDVoice::Voice1).setPulseWidth(pulse_width);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_code);
}

TEST(SIDDevice, setPulsWidthWriteLowFails) {
  uint16_t pulse_width = 0x0ABC;
  int write_error = -235;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x02).
      withParameter("value",0xBC).
      andReturnValue(write_error);

  returned_code = sid_device->voice(SIDVoice::Voice1).setPulseWidth(pulse_width);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_code);
}

TEST(SIDDevice, setPulsWidthVoice1) {
  uint16_t pulse_width = 0x0ABC;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x02).
      withParameter("value",0xBC).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x03).
      withParameter("value",0x0A).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice1).setPulseWidth(pulse_width);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, setReleaseVoice2) {
  uint8_t release = 0xC;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0D).
      withParameter("value",0x0C).
      andReturnValue(0);

  sid_device->voice(SIDVoice::Voice2).setRelease(release);

  mock().checkExpectations();
}

TEST(SIDDevice, setReleaseVoice1Fails) {
  uint8_t release = 0xB;
  int returned_value;
  int write_error = -23;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x06).
      withParameter("value",0x0B).
      andReturnValue(write_error);

  returned_value = sid_device->voice(SIDVoice::Voice1).setRelease(release);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_value);
}

TEST(SIDDevice, setReleaseVoice1) {
  uint8_t release = 0x4;
  int returned_value;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x06).
      withParameter("value",0x04).
      andReturnValue(0);

  returned_value = sid_device->voice(SIDVoice::Voice1).setRelease(release);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_value);
}

TEST(SIDDevice, setSustainVoice2) {
  uint8_t sustain = 0x3;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0D).
      withParameter("value",0x30).
      andReturnValue(0);

  sid_device->voice(SIDVoice::Voice2).setSustain(sustain);

  mock().checkExpectations();
}

TEST(SIDDevice, setSustainVoice1Fails) {
  uint8_t sustain = 0x4;
  int returned_value;
  int write_error = -23;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x06).
      withParameter("value",0x40).
      andReturnValue(write_error);

  returned_value = sid_device->voice(SIDVoice::Voice1).setSustain(sustain);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_value);
}

TEST(SIDDevice, setSustainVoice1) {
  uint8_t sustain = 0x4;
  int returned_value;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x06).
      withParameter("value",0x40).
      andReturnValue(0);

  returned_value = sid_device->voice(SIDVoice::Voice1).setSustain(sustain);;

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_value);
}

TEST(SIDDevice, setDecayTwiceDifferentBits) {
  uint8_t decay1 = 0x1;
  uint8_t decay2 = 0x2;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0C).
      withParameter("value",0x01).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0C).
      withParameter("value",0x02).
      andReturnValue(0);


  sid_device->voice(SIDVoice::Voice2).setDecay(decay1);
  sid_device->voice(SIDVoice::Voice2).setDecay(decay2);

  mock().checkExpectations();
}

TEST(SIDDevice, setAttackTwiceDifferentBits) {
  uint8_t attack1 = 0x1;
  uint8_t attack2 = 0x2;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0C).
      withParameter("value",0x10).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0C).
      withParameter("value",0x20).
      andReturnValue(0);

  sid_device->voice(SIDVoice::Voice2).setAttack(attack1);
  sid_device->voice(SIDVoice::Voice2).setAttack(attack2);

  mock().checkExpectations();
}

TEST(SIDDevice, setAttackAfterDecay) {
  uint8_t attack = 0x7;
  uint8_t decay = 0x4;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0C).
      withParameter("value",0x04).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0C).
      withParameter("value",0x74).
      andReturnValue(0);

  sid_device->voice(sid_synth::SIDVoice::Voice2).setDecay(decay);
  sid_device->voice(sid_synth::SIDVoice::Voice2).setAttack(attack);

  mock().checkExpectations();
}

TEST(SIDDevice, setDecayAfterAttack) {
  uint8_t attack = 0x7;
  uint8_t decay = 0x4;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0C).
      withParameter("value",0x70).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0C).
      withParameter("value",0x74).
      andReturnValue(0);

  sid_device->voice(sid_synth::SIDVoice::Voice2).setAttack(attack);
  sid_device->voice(sid_synth::SIDVoice::Voice2).setDecay(decay);

  mock().checkExpectations();
}


TEST(SIDDevice, setDecayVoice2) {
  uint8_t decay = 0x34;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0C).
      withParameter("value",0x04).
      andReturnValue(0);

  sid_device->voice(sid_synth::SIDVoice::Voice2).setDecay(decay);

  mock().checkExpectations();
}

TEST(SIDDevice, setDecayOverflowsMaximum) {
  uint8_t decay = 0x34;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x05).
      withParameter("value",0x04).
      andReturnValue(0);

  sid_device->voice(sid_synth::SIDVoice::Voice1).setDecay(decay);

  mock().checkExpectations();
}

TEST(SIDDevice, setDecayVoice1WriteFails) {
  uint8_t decay = 0x3;
  int write_error = -23;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x05).
      withParameter("value",0x03).
      andReturnValue(write_error);

  returned_code = sid_device->voice(sid_synth::SIDVoice::Voice1).setDecay(decay);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_code);
}

TEST(SIDDevice, setDecayVoice1) {
  uint8_t decay = 0x3;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x05).
      withParameter("value",0x03).
      andReturnValue(0);

  returned_code = sid_device->voice(sid_synth::SIDVoice::Voice1).setDecay(decay);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, setAttackVoice2) {
  uint8_t attack = 0x5;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0C).
      withParameter("value",0x50).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice2).setAttack(attack);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, setAttackVoice1WriteRegisterFails) {
  uint8_t attack = 0x3;
  int write_retunred_code = -23;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x05).
      withParameter("value",0x30).
      andReturnValue(write_retunred_code);

  returned_code = sid_device->voice(SIDVoice::Voice1).setAttack(attack);

  mock().checkExpectations();

  CHECK_EQUAL(write_retunred_code, returned_code);
}

TEST(SIDDevice, setAttackVoice1) {
  uint8_t attack = 0x3;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x05).
      withParameter("value",0x30).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice1).setAttack(attack);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, enableDisableGateVoice1WhenShapeWasSet) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x80).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x81).
      andReturnValue(0);

  sid_device->voice(SIDVoice::Voice1).enableShape(SIDVoiceShape::Noise, true);
  sid_device->voice(SIDVoice::Voice1).enableGate(true);

  mock().checkExpectations();
}

TEST(SIDDevice, enableDisableGateVoice1) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x01).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x00).
      andReturnValue(0);


  sid_device->voice(SIDVoice::Voice1).enableGate(true);
  sid_device->voice(SIDVoice::Voice1).enableGate(false);

  mock().checkExpectations();
}

TEST(SIDDevice, enableGateVoice1Fails) {
  int write_error = 23;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x01).
      andReturnValue(write_error);

  returned_code = sid_device->voice(SIDVoice::Voice1).enableGate(true);

  mock().checkExpectations();

  CHECK_EQUAL(write_error, returned_code);
}

TEST(SIDDevice, enableGateVoice1) {
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x01).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice1).enableGate(true);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, setVoice2Frequency) {
  SIDFrequency frequency = 0x1234;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x07).
      withParameter("value",0x34).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x08).
      withParameter("value",0x12).
      andReturnValue(0);

  sid_device->voice(SIDVoice::Voice2).setFrequency(frequency);

  mock().checkExpectations();
}

TEST(SIDDevice, setVoice1FrequencyWriteHighails) {
  SIDFrequency frequency = 0xabcd;
  int write_error_code = -33;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x00).
      withParameter("value",0xcd).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x01).
      withParameter("value",0xab).
      andReturnValue(write_error_code);

  returned_code = sid_device->voice(SIDVoice::Voice1).setFrequency(frequency);

  mock().checkExpectations();

  CHECK_EQUAL(write_error_code, returned_code);
}

TEST(SIDDevice, setVoice1FrequencyWriteLowFails) {
  SIDFrequency frequency = 0xabcd;
  int write_error_code = -234;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x00).
      withParameter("value",0xcd).
      andReturnValue(write_error_code);

  returned_code = sid_device->voice(SIDVoice::Voice1).setFrequency(frequency);

  mock().checkExpectations();

  CHECK_EQUAL(write_error_code, returned_code);
}

TEST(SIDDevice, setVoice1Frequency) {
  SIDFrequency frequency = 0xabcd;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x00).
      withParameter("value",0xcd).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x01).
      withParameter("value",0xab).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice1).setFrequency(frequency);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, enableDisableShapeVoice1Triangle) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x10).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x00).
      andReturnValue(0);

  sid_device->voice(SIDVoice::Voice1).enableShape(SIDVoiceShape::Triangle, true);
  sid_device->voice(SIDVoice::Voice1).enableShape(SIDVoiceShape::Triangle, false);

  mock().checkExpectations();
}

TEST(SIDDevice, enableMultipleShapeVoice1SawTriangle) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x20).
      andReturnValue(0);
  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x30).
      andReturnValue(0);

  sid_device->voice(SIDVoice::Voice1).enableShape(SIDVoiceShape::Saw, true);
  sid_device->voice(SIDVoice::Voice1).enableShape(SIDVoiceShape::Triangle, true);

  mock().checkExpectations();
}

TEST(SIDDevice, enableShapeVoice1SawWriteFails) {
  int returned_code;
  int mcp_returned_code = -22;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x20).
      andReturnValue(mcp_returned_code);

  returned_code = sid_device->voice(SIDVoice::Voice1).enableShape(SIDVoiceShape::Saw, true);

  mock().checkExpectations();

  CHECK_EQUAL(mcp_returned_code, returned_code);
}

TEST(SIDDevice, enableShapeVoice3Saw) {
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x12).
      withParameter("value",0x20).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice3).enableShape(SIDVoiceShape::Saw, true);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, enableShapeVoice2Saw) {
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x0B).
      withParameter("value",0x20).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice2).enableShape(SIDVoiceShape::Saw, true);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, enableShapeVoice1Saw) {
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x04).
      withParameter("value",0x20).
      andReturnValue(0);

  returned_code = sid_device->voice(SIDVoice::Voice1).enableShape(SIDVoiceShape::Saw, true);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, SetVolumeFails) {
  uint8_t volume = 3;
  int mcp_error = -234;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x18).
      withParameter("value",volume).
      andReturnValue(-234);

  returned_code = sid_device->setVolume(volume);

  mock().checkExpectations();

  CHECK_EQUAL(mcp_error, returned_code);
}

TEST(SIDDevice, SetVolume) {
  uint8_t volume = 3;
  int returned_code;
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  mock().expectOneCall("writeSIDRegister").onObject(SIDRegistersSink()).
      withParameter("reg", 0x18).
      withParameter("value",volume).
      andReturnValue(0);

  returned_code = sid_device->setVolume(volume);

  mock().checkExpectations();

  CHECK_EQUAL(0, returned_code);
}

TEST(SIDDevice, Constructor) {
  std::unique_ptr<SIDDevice> sid_device(BuildSIDDevice());

  CHECK(sid_device.get() != nullptr);
}