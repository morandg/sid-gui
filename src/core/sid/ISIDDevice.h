/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_I_SID_DEVICE_H_
#define _SID_GUI_I_SID_DEVICE_H_

#include <memory>

#include "SIDDefs.h"
#include "SIDVoice.h"
#include "SIDVoiceShape.h"
#include "SIDFilter.h"

namespace sid_synth {

enum class SIDModel {
  MOS6581,
  MOS8580
};

class ISIDDevice {
public:
  ISIDDevice() = default;
  virtual ~ISIDDevice() = default;

  static std::unique_ptr<ISIDDevice> CreateDummy();
  static std::unique_ptr<ISIDDevice> CreateMCP23017(const std::string& i2c_bus, uint8_t i2c_address);
  static std::unique_ptr<ISIDDevice> CreateReSID(const SIDModel& sid_model);

  virtual int open() = 0;
  virtual void close() = 0;
  virtual int setVolume(uint8_t volume) = 0;
  virtual SIDVoice& voice(SIDVoice::Id voice_num) = 0;
  virtual int setCutOffFrequency(SIDCutOffFrequency cut_off_frequency) = 0;
  virtual int setResonance(uint8_t resonance) = 0;
  virtual int enableFilter(SIDFilter::Filter filter, bool is_enabled) = 0;
};

}       // namespace
#endif  // _SID_GUI_I_SID_DEVICE_H_
