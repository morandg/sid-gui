/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../logging/LogMacros.h"

#include "ISIDDevice.h"
#include "SIDDevice.h"
#include "sid_synth_core_config.h"

#include "dummy/SIDRegistersSinkDummy.h"

#ifdef HAS_I2C_LINUX_SUPPORT
#include "mcp23017/SIDRegistersSinkMCP23017.h"
#endif

#ifdef HAS_RESID_SUPPORT
#include "resid/SIDDeviceReSID.h"
#include "resid/SIDRegistersSinkReSID.h"
#include "resid/AudioDeviceAlsa.h"
#endif

namespace sid_synth {

std::unique_ptr<ISIDDevice> ISIDDevice::CreateDummy() {
  return std::make_unique<SIDDevice>(std::make_unique<SIDRegistersSinkDummy>());
}

std::unique_ptr<ISIDDevice> ISIDDevice::CreateMCP23017(const std::string& i2c_bus, uint8_t i2c_address) {
#ifdef HAS_I2C_LINUX_SUPPORT
  return std::make_unique<SIDDevice>(std::make_unique<SIDRegistersSinkMCP23017>(i2c_bus, i2c_address));
#else
  (void)i2c_bus;
  (void)i2c_address;
  LOGER() << "No Linux i2c support, cannot create MCP23017 device";
  return nullptr;
#endif
}

std::unique_ptr<ISIDDevice> ISIDDevice::CreateReSID(const SIDModel& sid_model) {
#ifdef HAS_RESID_SUPPORT
  std::unique_ptr<SID> resid = std::make_unique<SID>();
  switch(sid_model) {
    case SIDModel::MOS6581:
      resid->set_chip_model(MOS6581);
      break;
    case SIDModel::MOS8580:
    default:
      resid->set_chip_model(MOS8580);
  }

  std::unique_ptr<ISIDDevice> sid_device = std::make_unique<SIDDevice>(std::make_unique<SIDRegistersSinkReSID>(*resid));
  std::unique_ptr<IAudioDevice> audio_thread = std::make_unique<AudioDeviceAlsa>(*resid);
  return std::make_unique<SIDDeviceReSID>(std::move(resid), std::move(sid_device), std::move(audio_thread));
#else
  (void)sid_model;
  LOGER() << "No reSID support, cannot create reSID device";
  return nullptr;
#endif
}

}       // namespace
