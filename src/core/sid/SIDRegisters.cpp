/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../logging/LogMacros.h"

#include "SIDDefs.h"
#include "SIDRegisters.h"

namespace sid_synth {

SIDRegisters::SIDRegisters(std::unique_ptr<ISIDRegistersSink> register_sink):
        _registers_sink(std::move(register_sink)) {
}

int SIDRegisters::reset() {
  _registers_sink->open();
  for(uint8_t reg = 0 ; reg < SID_REGISTER_MODE_VOL ; ++reg) {
    if(_registers_sink->writeSIDRegister(reg, 0x00)) {
      LOGER() << "Could not initialize SID register 0x" << std::hex << static_cast<int>(reg);
      return -1;
    }
  }

  return 0;
}

int SIDRegisters::set(int8_t reg, uint8_t value) {
  _cached_registers[reg] = value;

  return _registers_sink->writeSIDRegister(reg, _cached_registers[reg]);
}

int SIDRegisters::setHigh(uint8_t reg, uint8_t value) {
  _cached_registers[reg] = (_cached_registers[reg] & 0x0f) | (value << 4);

  return _registers_sink->writeSIDRegister(reg, _cached_registers[reg]);
}

int SIDRegisters::setLow(uint8_t reg, uint8_t value) {
  _cached_registers[reg] = (_cached_registers[reg] & 0xf0) | (value & 0x0f);

  return _registers_sink->writeSIDRegister(reg, _cached_registers[reg]);
}

int SIDRegisters::setBit(uint8_t reg, uint8_t bit, bool is_set_bit) {
  if(is_set_bit) {
    _cached_registers[reg] |= bit;
  } else {
    _cached_registers[reg] &= ~bit;
  }

  return _registers_sink->writeSIDRegister(reg, _cached_registers[reg]);
}

}       // namespace
