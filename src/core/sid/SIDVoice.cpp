/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SIDDefs.h"
#include "SIDVoice.h"

namespace sid_synth {

SIDVoice::SIDVoice(Id voice, SIDRegisters& registers):
    _voice(voice),
    _registers(registers) {
};

int SIDVoice::enableShape(const SIDVoiceShape& shape, bool is_enabled)  {
  return _registers.setBit(controlRegister(), shape.shapeBit(), is_enabled);
}

int SIDVoice::setFrequency(SIDFrequency frequency) {
  int returned_code;
  uint8_t freq_low = frequency & 0x00ff;
  uint8_t freq_high = (frequency & 0xff00) >> 8;

  returned_code = _registers.set(frequencyLowRegister(), freq_low);
  if(returned_code) {
    return returned_code;
  }
  return _registers.set(frequencyHighRegister(), freq_high);
}

int SIDVoice::enableGate(bool is_enabled) {
  return _registers.setBit(controlRegister(), SID_REGISTER_CTL_BIT_GATE, is_enabled);
}

int SIDVoice::setAttack(uint8_t attack)  {
  return _registers.setHigh(attackDecayRegister(), attack);
}

int SIDVoice::setDecay(uint8_t decay)  {
  return _registers.setLow(attackDecayRegister(), decay);
}

int SIDVoice::setSustain(uint8_t sustain)  {
  return _registers.setHigh(sustainReleaseRegister(), sustain);
}

int SIDVoice::setRelease(uint8_t release)  {
  return _registers.setLow(sustainReleaseRegister(), release);
}

int SIDVoice::setPulseWidth(uint16_t width) {
  int returned_code;

  returned_code = _registers.set(pulseWidthLowRegister(), width);
  if(returned_code) {
    return returned_code;
  }
  return _registers.setLow(pulseWidthHighRegister(), width >> 8);
}

int SIDVoice::enableRing(bool is_enabled) {
  return _registers.setBit(controlRegister(), SID_REGISTER_CTL_BIT_RING, is_enabled);
}

int SIDVoice::enableSync(bool is_enabled) {
  return _registers.setBit(controlRegister(), SID_REGISTER_CTL_BIT_SYNC, is_enabled);
}

int SIDVoice::enableFilter(bool is_enabled) {
  return _registers.setBit(SID_REGISTER_RES_FILT, enableFilterBit(), is_enabled);
}

uint8_t SIDVoice::baseRegister() const {
  switch(_voice) {
    case Voice1:
      return SID_REGISTER_VOICE1_BASE;
    case Voice2:
      return SID_REGISTER_VOICE2_BASE;
    case Voice3:
    default:  // Compiler warning
      return SID_REGISTER_VOICE3_BASE;
  }
}

uint8_t SIDVoice::controlRegister() const {
  return baseRegister() + SID_REGISTER_VOICE_CTL_OFFSET;
}

uint8_t SIDVoice::frequencyLowRegister() const {
  return baseRegister() + SID_REGISTER_VOICE_FREQ_LOW_OFFSET;
}

uint8_t SIDVoice::frequencyHighRegister() const {
  return baseRegister() + SID_REGISTER_VOICE_FREQ_HIGH_OFFSET;
}

uint8_t SIDVoice::attackDecayRegister() const {
  return baseRegister() + SID_REGISTER_VOICE_ATK_DCY_OFFSET;
}

uint8_t SIDVoice::sustainReleaseRegister() const {
  return baseRegister() + SID_REGISTER_VOICE_STN_RIS_OFFSET;
}

uint8_t SIDVoice::pulseWidthLowRegister() const {
  return baseRegister() + SID_REGISTER_VOICE_PULSE_WIDTH_LOW_OFFSET;
}

uint8_t SIDVoice::pulseWidthHighRegister() const {
  return baseRegister() + SID_REGISTER_VOICE_PULSE_WIDTH_HIGH_OFFSET;
}

uint8_t SIDVoice::enableFilterBit() const {
  switch(_voice) {
    case Voice1:
      return SID_REGISTER_RES_FILT_BIT_VOICE1;
    case Voice2:
      return SID_REGISTER_RES_FILT_BIT_VOICE2;
    case Voice3:
    default:  // Compiler warning
      return SID_REGISTER_RES_FILT_BIT_VOICE3;
  }
}

SIDVoice::operator SIDVoice::Id() const {
  return _voice;
}
  int enableVoiceFilter(bool is_enabled);

std::ostream& operator<<(std::ostream& os, const SIDVoice& voice) {
  switch(voice) {
    case SIDVoice::Voice1:
      os << "Voice 1";
      break;
    case SIDVoice::Voice2:
      os << "Voice 2";
      break;
    case SIDVoice::Voice3:
      os << "Voice 3";
      break;
  }

  return os;
}

}       // namespace
