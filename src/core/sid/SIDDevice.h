/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_SID_DEVICE_H_
#define _SID_GUI_SID_DEVICE_H_

#include "ISIDRegistersSink.h"
#include "SIDRegisters.h"
#include "ISIDDevice.h"

namespace sid_synth {

class SIDDevice: public ISIDDevice {
public:
  SIDDevice(std::unique_ptr<ISIDRegistersSink> registers_sink);
  virtual ~SIDDevice();

  virtual int open() override;
  virtual void close() override;
  virtual int setVolume(uint8_t volume) override;
  SIDVoice& voice(SIDVoice::Id voice_num) override;
  virtual int setCutOffFrequency(SIDCutOffFrequency cut_off_frequency) override;
  virtual int setResonance(uint8_t resonance) override;
  virtual int enableFilter(SIDFilter::Filter filter, bool is_enabled) override;

private:
  SIDRegisters _registers;
  SIDVoice _voice1;
  SIDVoice _voice2;
  SIDVoice _voice3;
};

}       // namespace
#endif  // _SID_GUI_SID_DEVICE_H_
