/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_SID_SID_REGISTERS_H_
#define _SID_GUI_SID_SID_REGISTERS_H_

#include <unordered_map>
#include <memory>

#include "ISIDRegistersSink.h"

namespace sid_synth {

class SIDRegisters {
public:
  SIDRegisters(std::unique_ptr<ISIDRegistersSink> register_sink);
  ~SIDRegisters() = default;

  int reset();
  int set(int8_t reg, uint8_t value);
  int setHigh(uint8_t reg, uint8_t value);
  int setLow(uint8_t reg, uint8_t value);
  int setBit(uint8_t reg, uint8_t bit, bool is_set_bit);

private:
  std::unique_ptr<ISIDRegistersSink> _registers_sink;
  std::unordered_map<uint8_t, uint8_t> _cached_registers;
};

}       // namespace
#endif  // _SID_GUI_SID_SID_REGISTERS_H_
