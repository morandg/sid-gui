/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_SID_DEFS_H_
#define _SID_GUI_SID_DEFS_H_

#include <cstdint>
#include <limits>

namespace sid_synth {

typedef uint16_t SIDFrequency;
typedef uint16_t SIDCutOffFrequency;

static constexpr uint8_t SID_REGISTER_VOICE1_BASE = 0x00;
static constexpr uint8_t SID_REGISTER_VOICE2_BASE = 0x07;
static constexpr uint8_t SID_REGISTER_VOICE3_BASE = 0x0E;
static constexpr uint8_t SID_REGISTER_FC_LOW = 0x15;
static constexpr uint8_t SID_REGISTER_FC_HIGH = 0x16;
static constexpr uint8_t SID_REGISTER_RES_FILT = 0x17;
static constexpr uint8_t SID_REGISTER_MODE_VOL = 0x18;

static constexpr uint8_t SID_REGISTER_VOICE_FREQ_LOW_OFFSET = 0x00;
static constexpr uint8_t SID_REGISTER_VOICE_FREQ_HIGH_OFFSET = 0x01;
static constexpr uint8_t SID_REGISTER_VOICE_PULSE_WIDTH_LOW_OFFSET = 0x02;
static constexpr uint8_t SID_REGISTER_VOICE_PULSE_WIDTH_HIGH_OFFSET = 0x03;
static constexpr uint8_t SID_REGISTER_VOICE_CTL_OFFSET = 0x04;
static constexpr uint8_t SID_REGISTER_VOICE_ATK_DCY_OFFSET = 0x05;
static constexpr uint8_t SID_REGISTER_VOICE_STN_RIS_OFFSET = 0x06;

static constexpr uint8_t SID_REGISTER_CTL_BIT_GATE = 1;
static constexpr uint8_t SID_REGISTER_CTL_BIT_SYNC = 1 << 1;
static constexpr uint8_t SID_REGISTER_CTL_BIT_RING = 1 << 2;
static constexpr uint8_t SID_REGISTER_CTL_BIT_TRIANGLE = 1 << 4;
static constexpr uint8_t SID_REGISTER_CTL_BIT_SAW = 1 << 5;
static constexpr uint8_t SID_REGISTER_CTL_BIT_PULSE = 1 << 6;
static constexpr uint8_t SID_REGISTER_CTL_BIT_NOISE = 1 << 7;

static constexpr uint8_t SID_REGISTER_RES_FILT_BIT_VOICE1 = 1;
static constexpr uint8_t SID_REGISTER_RES_FILT_BIT_VOICE2 = 1 << 1;
static constexpr uint8_t SID_REGISTER_RES_FILT_BIT_VOICE3 = 1 << 2;

static constexpr uint8_t SID_REGISTER_MODE_VOL_BIT_LP = 1 << 4;
static constexpr uint8_t SID_REGISTER_MODE_VOL_BIT_BP = 1 << 5;
static constexpr uint8_t SID_REGISTER_MODE_VOL_BIT_HP = 1 << 6;
static constexpr uint8_t SID_REGISTER_MODE_VOL_BIT_3_OFF = 1 << 7;

static constexpr SIDFrequency SID_MAX_FREQUENCY = std::numeric_limits<SIDFrequency>::max();
static constexpr SIDCutOffFrequency SID_MAX_CUT_OFF_FREQUENCY = 0x07FF;
static constexpr uint8_t SID_MAX_4_BITS_REGISTER_VALUE = 0x0f;
static constexpr uint16_t SID_MAX_PULSE_WIDTH = 0x0fff;

}       // namespace
#endif  // _SID_GUI_SID_DEFS_H_
