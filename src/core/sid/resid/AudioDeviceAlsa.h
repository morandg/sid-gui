/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_AUDIO_DEVICE_ALSA_H_
#define _SID_GUI_AUDIO_DEVICE_ALSA_H_

#include <thread>

#include <alsa/asoundlib.h>

#include <resid-0.16/sid.h>

#include "IAudioDevice.h"

namespace sid_synth {

class AudioDeviceAlsa: public IAudioDevice {
public:
  AudioDeviceAlsa(SID& resid);
  virtual ~AudioDeviceAlsa();

  virtual int open() override;
  virtual void close() override;

private:
  SID& _resid;
  snd_pcm_t* _alsa_pcm;
  bool _is_running;
  std::thread _audio_sink_thread;

  int configureReSID();
  int openAlsa();
  int recoverXrun();
  int sinkSIDAudio(snd_pcm_sframes_t frames_count);
  void sinkThread();
};

}       // namespace
#endif  // _SID_GUI_AUDIO_DEVICE_ALSA_H_
