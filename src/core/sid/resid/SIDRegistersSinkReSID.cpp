/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iomanip>

#include "../../logging/LogMacros.h"
#include "SIDRegistersSinkReSID.h"

namespace sid_synth {

SIDRegistersSinkReSID::SIDRegistersSinkReSID(SID& reSID):
    _reSID(reSID) {
}

int SIDRegistersSinkReSID::open() {
  return 0;
}

void SIDRegistersSinkReSID::close() {
}

int SIDRegistersSinkReSID::writeSIDRegister(uint8_t reg, uint8_t value) {
  LOGDB() << "Write reSID register 0x" << std::setfill('0') << std::setw(2) << static_cast<int>(reg) <<
      ":0x" << std::hex << std::setw(2) << static_cast<int>(value);
  _reSID.write(reg, value);
  return 0;
}

}       // namespace
