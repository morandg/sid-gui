/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_SID_DEVICE_RESID_H_
#define _SID_GUI_SID_DEVICE_RESID_H_

#include <resid-0.16/sid.h>

#include "IAudioDevice.h"
#include "../ISIDDevice.h"

namespace sid_synth {

class SIDDeviceReSID: public ISIDDevice {
public:
  SIDDeviceReSID(
      std::unique_ptr<SID> resid,
      std::unique_ptr<ISIDDevice> sid_device,
      std::unique_ptr<IAudioDevice> audio_device);
  virtual ~SIDDeviceReSID();

  int open() override;
  void close() override;
  int setVolume(uint8_t volume) override;
  SIDVoice& voice(SIDVoice::Id voice_num) override;
  int setCutOffFrequency(SIDCutOffFrequency cut_off_frequency) override;
  int setResonance(uint8_t resonance) override;
  int enableFilter(SIDFilter::Filter filter, bool is_enabled) override;

private:
  std::unique_ptr<SID> _resid;
  std::unique_ptr<ISIDDevice> _sid_device;
  std::unique_ptr<IAudioDevice> _audio_device;
};

}       // namespace
#endif  // _SID_GUI_SID_DEVICE_H_
