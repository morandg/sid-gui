/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include "../../logging/LogMacros.h"
#include "SIDDeviceReSID.h"

namespace sid_synth {

SIDDeviceReSID::SIDDeviceReSID(
      std::unique_ptr<SID> resid,
      std::unique_ptr<ISIDDevice> sid_device,
      std::unique_ptr<IAudioDevice> audio_device):
    _resid(std::move(resid)),
    _sid_device(std::move(sid_device)),
    _audio_device(std::move(audio_device)) {
  assert(_sid_device);
  assert(_resid);
  assert(_audio_device);
}

SIDDeviceReSID::~SIDDeviceReSID() {
  LOGDB() << "SIDDeviceReSID::" << __func__;
  close();
}

int SIDDeviceReSID::open() {
  int returned_code;

  returned_code = _sid_device->open();
  if(returned_code) {
    return returned_code;
  }

  return _audio_device->open();
}

void SIDDeviceReSID::close() {
  _audio_device->close();
  _sid_device->close();
}

int SIDDeviceReSID::setVolume(uint8_t volume) {
  return _sid_device->setVolume(volume);
}

SIDVoice& SIDDeviceReSID::voice(SIDVoice::Id voice_num) {
  return _sid_device->voice(voice_num);
}

int SIDDeviceReSID::setCutOffFrequency(SIDCutOffFrequency cut_off_frequency) {
  return _sid_device->setCutOffFrequency(cut_off_frequency);
}

int SIDDeviceReSID::setResonance(uint8_t resonance) {
  return _sid_device->setResonance(resonance);
}

int SIDDeviceReSID::enableFilter(SIDFilter::Filter filter, bool is_enabled) {
  return _sid_device->enableFilter(filter, is_enabled);
};

}       // namespace
