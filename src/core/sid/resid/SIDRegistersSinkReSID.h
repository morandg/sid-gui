/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_SID_REGISTERS_SINK_RESID_H_
#define _SID_GUI_SID_REGISTERS_SINK_RESID_H_

#include <resid-0.16/sid.h>

#include "../ISIDRegistersSink.h"

namespace sid_synth {

class SIDRegistersSinkReSID: public ISIDRegistersSink {
public:
  SIDRegistersSinkReSID(SID& reSID);
  virtual ~SIDRegistersSinkReSID() = default;

  virtual int open() override;
  virtual void close() override;
  virtual int writeSIDRegister(uint8_t reg, uint8_t value) override;

private:
  SID& _reSID;
};

}       // namespace
#endif  // _SID_GUI_SID_REGISTERS_SINK_RESID_H_
