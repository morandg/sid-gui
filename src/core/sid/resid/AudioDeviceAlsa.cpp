/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../../logging/LogMacros.h"
#include "AudioDeviceAlsa.h"

namespace sid_synth {

static constexpr unsigned int SAMPLE_RATE = 48000;

AudioDeviceAlsa::AudioDeviceAlsa(SID& resid):
    _resid(resid),
    _alsa_pcm(nullptr),
    _is_running(false) {
}

AudioDeviceAlsa::~AudioDeviceAlsa() {
  close();
}

int AudioDeviceAlsa::open() {
  int returned_code;

  close();
  returned_code = configureReSID();
  if(returned_code) {
    return returned_code;
  }
  return openAlsa();
}

void AudioDeviceAlsa::close() {
  _is_running = false;
  try {
    _audio_sink_thread.join();
  } catch (...) {
  }

  if(_alsa_pcm) {
    snd_pcm_close(_alsa_pcm);
    _alsa_pcm = nullptr;
  }
}


int AudioDeviceAlsa::configureReSID() {
  // Use a clock freqency of 985248Hz for PAL C64, 1022730Hz for NTSC C64.q
  if(!_resid.set_sampling_parameters(985248, SAMPLE_INTERPOLATE, SAMPLE_RATE)) {
    LOGER() << "Could not configure reSID";
    return -1;
  }

  return 0;
}

int AudioDeviceAlsa::openAlsa() {
  snd_pcm_sw_params_t *sw_params;
  int alsa_error;

  alsa_error = snd_pcm_open(&_alsa_pcm, "default", SND_PCM_STREAM_PLAYBACK, 0);
  if(alsa_error) {
    LOGER() << "Cannot open alsa device \"default\": " << snd_strerror(alsa_error);
    return -1;
  }

  alsa_error = snd_pcm_set_params(
    _alsa_pcm,
    SND_PCM_FORMAT_S16,
    SND_PCM_ACCESS_RW_INTERLEAVED,
    2,              // Mono
    SAMPLE_RATE,    // Hz
    1,              // Allow soft resample
    20000);         // Latency us
  if(alsa_error < 0) {
    LOGER() << "Cannot configure alsa device : " << snd_strerror(alsa_error);
    return -1;
  }

  alsa_error = snd_pcm_sw_params_malloc(&sw_params);
  if(alsa_error < 0) {
    LOGER() << "Cannot allocate ALSA software parameters: " << snd_strerror(alsa_error);
    snd_pcm_sw_params_free(sw_params);
    return -1;
  }
  alsa_error = snd_pcm_sw_params_current(_alsa_pcm, sw_params);
  if(alsa_error < 0) {
    LOGER() << "Cannot get current ALSA software parameters: " << snd_strerror(alsa_error);
    snd_pcm_sw_params_free(sw_params);
    return -1;
  }
  alsa_error = snd_pcm_sw_params_set_start_threshold(_alsa_pcm, sw_params, 0);
  if(alsa_error) {
    LOGER() << "Cannot set ALSA start mode software parameter: " << snd_strerror(alsa_error);
    snd_pcm_sw_params_free(sw_params);
    return -1;
  }
  alsa_error = snd_pcm_sw_params(_alsa_pcm, sw_params);
  if(alsa_error) {
    LOGER() << "Cannot set ALSA software parameters: " << snd_strerror(alsa_error);
    snd_pcm_sw_params_free(sw_params);
    return -1;
  }
  snd_pcm_sw_params_free(sw_params);

  alsa_error = snd_pcm_prepare(_alsa_pcm);
  if(alsa_error < 0) {
    LOGER() << "Cannot prepare ALSA device: " << snd_strerror(alsa_error);
    return -1;
  }

  _is_running = true;
  _audio_sink_thread = std::thread(&AudioDeviceAlsa::sinkThread, this);

  return 0;
}

int AudioDeviceAlsa::recoverXrun() {
  int alsa_error;

  LOGWA() << "XRUN occured!";
  while(true) {
    snd_pcm_state_t pcm_state = snd_pcm_state(_alsa_pcm);

    if(pcm_state == SND_PCM_STATE_SETUP ||
        pcm_state == SND_PCM_STATE_OPEN ||
        pcm_state == SND_PCM_STATE_PREPARED ||
        pcm_state == SND_PCM_STATE_XRUN) {
      break;
    } else {
      std::this_thread::sleep_for(std::chrono::microseconds(100));
    }
  }
  
  alsa_error = snd_pcm_prepare(_alsa_pcm);
  if(alsa_error < 0) {
    LOGER() << "Cannot prepare ALSA device: " << snd_strerror(alsa_error);
    return -1;
  }

  return 0;
  
}

int AudioDeviceAlsa::sinkSIDAudio(snd_pcm_sframes_t frames_count) {
  short audio_buffer[frames_count];
  int generated_samples;
  cycle_count sid_cycle;

  memset(audio_buffer, 0, sizeof(short) * frames_count);
  
  // How many clock cycles?
  sid_cycle = 1000;
  while(sid_cycle) {
    int returned_code;

    generated_samples = _resid.clock(sid_cycle, audio_buffer, frames_count);
    returned_code = snd_pcm_writei(_alsa_pcm, audio_buffer, generated_samples * 2);
    if(returned_code < 0) {
      if(returned_code == -EPIPE) {
        return recoverXrun();
      } else {
        LOGER() << "Could not write ALSA audio data: " << snd_strerror(returned_code);
        return -1;
      }
    }
  }

  return 0;
}

void AudioDeviceAlsa::sinkThread() {
  LOGDB() << "ALSA sink thread started";

  while(_is_running) {
    int returned_code;
    snd_pcm_sframes_t available_chunks;

    returned_code = snd_pcm_wait(_alsa_pcm, 500);
    if(returned_code == 0) {
      continue;
    } else if(returned_code < 0) {
      LOGER() << "snd pcm retunred " << returned_code << ": " << snd_strerror(returned_code);
      recoverXrun();
      break;
    }

    available_chunks = snd_pcm_avail_update(_alsa_pcm);
    if(available_chunks < 0) {
      LOGER() << "Get available ALSA chunk failed: " << snd_strerror(available_chunks);
      if(recoverXrun()) {
        break;
      } else {
        continue;
      }
    } else if(available_chunks > 0) {
      if(available_chunks > 2048) {
        available_chunks = 2048;
      }
      if(sinkSIDAudio(available_chunks)) {
        break;
      }
    }
  }
  _is_running = false;

  LOGDB() << "ALSA sink thread stopped";
}

}       // namespace
