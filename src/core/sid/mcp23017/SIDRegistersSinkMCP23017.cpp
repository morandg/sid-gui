/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <linux/i2c-dev.h>

#include <sys/ioctl.h>

#include <fcntl.h>
#include <unistd.h>

#include <cstring>
#include <cerrno>

#include "../../logging/LogMacros.h"
#include "SIDRegistersSinkMCP23017.h"

namespace sid_synth {

SIDRegistersSinkMCP23017::SIDRegistersSinkMCP23017(const std::string& i2c_bus, uint8_t i2c_address):
    _i2c_bus(i2c_bus),
    _i2c_address(i2c_address),
    _i2c_file_desriptor(-1) {
}

SIDRegistersSinkMCP23017::~SIDRegistersSinkMCP23017() {
  close();
}

int SIDRegistersSinkMCP23017::open() {
  close();

  LOGDB() << "Opening MCP23017 i2c device...";

  // Not sure if O_WRONLY would also work
  _i2c_file_desriptor = ::open(_i2c_bus.c_str(), O_RDWR);
  if(_i2c_file_desriptor < 0) {
    LOGER() << "Could not open i2c bus " << _i2c_bus << ": " << strerror(errno);
    return -1;
  }

  if(ioctl(_i2c_file_desriptor, I2C_SLAVE, static_cast<int>(_i2c_address))) {
    LOGER() << "Could not set i2c slave address on bus " << _i2c_bus << ": " << strerror(errno);
    close();
    return -1;
  }

  if(writeMcpRegister(IODIRA, 0x00) ||
      writeMcpRegister(IODIRB, 0x00)) {
    close();
    return -1;
  }

  LOGIN() << "MCP23017 i2c device ready!";

  return 0;
}

void SIDRegistersSinkMCP23017::close() {
  if(_i2c_file_desriptor >= 0) {
    LOGDB() << "Closing MCP23017 i2c device ...";
    ::close(_i2c_file_desriptor);
    _i2c_file_desriptor = -1;
  }
}


int SIDRegistersSinkMCP23017::writeSIDRegister(uint8_t reg, uint8_t value) {
  static constexpr uint8_t CS_BIT = 0x20;
  if(writeMcpRegister(GPIOB, value) ||
      writeMcpRegister(GPIOA, CS_BIT | reg) ||
      writeMcpRegister(GPIOA, reg)) {
    return -1;
  }

  return 0;
}

int SIDRegistersSinkMCP23017::writeMcpRegister(uint8_t reg, uint8_t value) {
  static constexpr unsigned int buffer_size = 2;
  uint8_t buffer[buffer_size] = {reg, value};

  if(write(_i2c_file_desriptor, buffer, buffer_size) != buffer_size) {
    LOGER() << "Could not write on MCP23017 device: " << strerror(errno);
    return -1;
  }

  return 0;
}

}       // namespace
