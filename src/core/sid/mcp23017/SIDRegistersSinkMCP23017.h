/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_SID_REGISTERS_SINK_MCP_23017_H_
#define _SID_GUI_SID_REGISTERS_SINK_MCP_23017_H_

#include <string>

#include "../ISIDRegistersSink.h"

namespace sid_synth {

class SIDRegistersSinkMCP23017: public ISIDRegistersSink {
public:
  static constexpr uint8_t IODIRA = 0x00;
  static constexpr uint8_t IODIRB = 0x01;
  static constexpr uint8_t GPIOA = 0x12;
  static constexpr uint8_t GPIOB = 0x13;

  SIDRegistersSinkMCP23017(const std::string& i2c_bus, uint8_t i2c_address);
  virtual ~SIDRegistersSinkMCP23017();

  virtual int open() override;
  virtual void close() override;
  virtual int writeSIDRegister(uint8_t reg, uint8_t value) override;

private:
  std::string _i2c_bus;
  uint8_t _i2c_address;
  int _i2c_file_desriptor;

  int writeMcpRegister(uint8_t reg, uint8_t value);
};

}       // namespace
#endif  // _SID_GUI_SID_REGISTERS_SINK_MCP_23017_H_
