/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SIDDefs.h"
#include "SIDFilter.h"

namespace sid_synth {

SIDFilter::SIDFilter(Filter filter):
    _filter(filter) {
}

uint8_t SIDFilter::filterBit() const {
  switch(_filter) {
    case LowPass:
      return SID_REGISTER_MODE_VOL_BIT_LP;
    case BandPass:
      return SID_REGISTER_MODE_VOL_BIT_BP;
    case HighPass:
      return SID_REGISTER_MODE_VOL_BIT_HP;
    case Voice3Off:
    default:
      return SID_REGISTER_MODE_VOL_BIT_3_OFF;
  }
}

SIDFilter::operator SIDFilter::Filter() const {
  return _filter;
}

}       // namespace
