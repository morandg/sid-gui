/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_SID_VOICE_H_
#define _SID_GUI_SID_VOICE_H_

#include <ostream>

#include "SIDDefs.h"
#include "SIDRegisters.h"
#include "SIDVoiceShape.h"

namespace sid_synth {

class SIDVoice {
public:
  enum Id {
    Voice1,
    Voice2,
    Voice3
  };

  SIDVoice(Id voice, SIDRegisters& registers);
  ~SIDVoice() = default;

  int enableShape(const SIDVoiceShape& shape, bool is_enabled) ;
  int setFrequency(SIDFrequency frequency);
  int enableGate(bool is_enabled);
  int setAttack(uint8_t attack);
  int setDecay(uint8_t decay);
  int setSustain(uint8_t sustain);
  int setRelease(uint8_t release);
  int setPulseWidth(uint16_t width);
  int enableRing(bool is_enabled);
  int enableSync(bool is_enabled);
  int enableFilter(bool is_enabled);

  operator Id() const;
  explicit operator bool() = delete;

private:
  Id _voice;
  SIDRegisters& _registers;

  uint8_t baseRegister() const;
  uint8_t controlRegister() const;
  uint8_t frequencyLowRegister() const;
  uint8_t frequencyHighRegister() const;
  uint8_t attackDecayRegister() const;
  uint8_t sustainReleaseRegister() const;
  uint8_t pulseWidthLowRegister() const;
  uint8_t pulseWidthHighRegister() const;
  uint8_t enableFilterBit() const;
};

std::ostream& operator<<(std::ostream& os, const SIDVoice& voice);

}       // namespace
#endif  // _SID_GUI_SID_VOICE_H_
