/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iomanip>

#include "../logging/LogMacros.h"

#include "SIDDevice.h"

namespace sid_synth {

SIDDevice::SIDDevice(std::unique_ptr<ISIDRegistersSink> registers_sink):
    _registers(std::move(registers_sink)),
    _voice1(SIDVoice::Voice1, _registers),
    _voice2(SIDVoice::Voice2, _registers),
    _voice3(SIDVoice::Voice3, _registers) {
}

SIDDevice::~SIDDevice() {
}

int SIDDevice::open() {
  if(_registers.reset()) {
    return -1;
  }

  LOGIN() << "SID device ready";

  return 0;
}

void SIDDevice::close() {
}

int SIDDevice::setVolume(uint8_t volume) {
  return _registers.setLow(SID_REGISTER_MODE_VOL, volume);
}

SIDVoice& SIDDevice::voice(SIDVoice::Id voice_num) {
  switch (voice_num) {
    case SIDVoice::Voice1:
      return _voice1;
    case SIDVoice::Voice2:
      return _voice2;
    case SIDVoice::Voice3:
    default:
      return _voice3;
  }
}

int SIDDevice::setCutOffFrequency(SIDCutOffFrequency cut_off_frequency) {
  int returned_code;

  returned_code = _registers.set(SID_REGISTER_FC_LOW, cut_off_frequency & 0x0007);
  if(returned_code) {
    return returned_code;
  }
  return _registers.set(SID_REGISTER_FC_HIGH, cut_off_frequency >> 3);
}

int SIDDevice::setResonance(uint8_t resonance) {
  return _registers.setHigh(SID_REGISTER_RES_FILT, resonance);
}

int SIDDevice::enableFilter(SIDFilter::Filter filter, bool is_enabled) {
  SIDFilter filter_bit(filter);

  return _registers.setBit(SID_REGISTER_MODE_VOL, filter_bit.filterBit(), is_enabled);
}

}       // namespace
