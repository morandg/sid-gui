/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SIDDefs.h"
#include "SIDVoiceShape.h"

namespace sid_synth {

SIDVoiceShape::SIDVoiceShape(VoiceShape shape):
    _shape(shape) {
};

uint8_t SIDVoiceShape::shapeBit() const {
  switch(_shape) {
    case Triangle:
      return SID_REGISTER_CTL_BIT_TRIANGLE;
    case Saw:
      return SID_REGISTER_CTL_BIT_SAW;
    case Pulse:
      return SID_REGISTER_CTL_BIT_PULSE;
    case Noise:
    default:  // Compiler warning
      return SID_REGISTER_CTL_BIT_NOISE;
  }
}

SIDVoiceShape::operator SIDVoiceShape::VoiceShape() const {
  return _shape;
}

std::ostream& operator<<(std::ostream& os, const SIDVoiceShape& shape) {
  switch(shape) {
    case SIDVoiceShape::Triangle:
      os << "Triangle";
      break;
    case SIDVoiceShape::Saw:
      os << "Saw";
      break;
    case SIDVoiceShape::Pulse:
      os << "Pulse";
      break;
    case SIDVoiceShape::Noise:
      os << "Noise";
      break;
  }

  return os;
}

}       // namespace
