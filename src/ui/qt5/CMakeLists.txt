set(QT_GUI_APP_NAME ${PROJECT_NAME}-qt5)
set(QT_GUI_SOURCES
    dialogs/QSIDCreateDialog.cpp
    QSIDVoice.cpp
    QSIDDevice.cpp
    QSIDVoicePanel.cpp
    QSIDFilterPanel.cpp
    QSIDPanel.cpp
    QSIDGui.cpp
    main.cpp)

if(${HAS_RESID_SUPPORT})
    list(APPEND
        QT_GUI_SOURCES
        dialogs/resid/QCreateResidForm.cpp
        dialogs/resid/QSIDCreateDialogReSID.cpp)
endif()

if(${HAS_I2C_LINUX_SUPPORT})
    list(APPEND
        QT_GUI_SOURCES
        dialogs/mcp/QCreateMCPSIDForm.cpp
        dialogs/mcp/QSIDCreateDialogMCP.cpp)
endif()

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

add_executable(
    ${QT_GUI_APP_NAME}
    ${QT_GUI_SOURCES})

target_link_libraries(
    ${QT_GUI_APP_NAME}
    ${APP_CORE_LIB_NAME}
    ${APP_CORE_LIB_EXTRA_LIB}
    Qt5::Widgets)

install(TARGETS ${QT_GUI_APP_NAME} DESTINATION bin/)