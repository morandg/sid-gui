/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QSlider>
#include <QLabel>
#include <QFormLayout>

#include "QSIDFilterPanel.h"
#include "QSIDPanel.h"

namespace sid_synth {

QSIDPanel::QSIDPanel(std::unique_ptr<ISIDDevice> sid_device, QWidget* parent) :
    QWidget(parent),
    _main_layout(new QVBoxLayout(this)),
    _q_sid_device(new QSIDDevice(std::move(sid_device), this)) {
  connect(_q_sid_device, &QSIDDevice::deviceOpened, this, &QSIDPanel::sidDeviceOpened);
  _q_sid_device->open();
}

void QSIDPanel::sidDeviceOpened(bool is_succes) {
  if(is_succes) {
    addSIDWidgets();
  } else {
    _main_layout->addWidget(new QLabel("Error opening SID device", this));
  }
}

void QSIDPanel::addSIDWidgets() {
  _main_layout->addWidget(createVolumePanel());
  _main_layout->addWidget(createVoicePanel("Voice 1", _q_sid_device->voice(SIDVoice::Voice1)));
  _main_layout->addWidget(createVoicePanel("Voice 2", _q_sid_device->voice(SIDVoice::Voice2)));
  _main_layout->addWidget(createVoicePanel("Voice 3", _q_sid_device->voice(SIDVoice::Voice3)));
  _main_layout->addWidget(createFilterPanel());

  _main_layout->addStretch();
}

QWidget* QSIDPanel::createVoicePanel(const QString& label, QSIDVoice* voice) {
  QGroupBox* voice_group_box = new QGroupBox(label, this);
  QVBoxLayout* main_layout = new QVBoxLayout(voice_group_box);
  QSIDVoicePanel* voice_panel = new QSIDVoicePanel(voice, voice_group_box);

  main_layout->addWidget(voice_panel);

  return voice_group_box;
}

QWidget* QSIDPanel::createFilterPanel() {
  QGroupBox* filter_box = new QGroupBox("Filter", this);
  QVBoxLayout* main_layout = new QVBoxLayout(filter_box);

  QSIDFilterPanel* filter_panel = new QSIDFilterPanel(this);
  connect(filter_panel, &QSIDFilterPanel::cutOffFrequencyChanged, _q_sid_device, &QSIDDevice::setCutOffFrequency);
  connect(filter_panel, &QSIDFilterPanel::resonanceChanged, _q_sid_device, &QSIDDevice::setResonance);
  connect(filter_panel, &QSIDFilterPanel::enableVoiceFilterChanged, this, [this](SIDVoice::Id voice, bool is_enabled) {
    _q_sid_device->voice(voice)->enableFilter(is_enabled);
  });
  connect(filter_panel, &QSIDFilterPanel::enableFilterChanged, _q_sid_device, &QSIDDevice::enableFilter);

  main_layout->addWidget(filter_panel);

  return filter_box;
}

QWidget* QSIDPanel::createVolumePanel() {
  QWidget* volume_widget = new QWidget(this);
  QFormLayout* main_layout = new QFormLayout(volume_widget);

  QSlider* volume_slider = new QSlider(Qt::Horizontal, this);
  volume_slider->setMinimum(0);
  volume_slider->setMaximum(SID_MAX_4_BITS_REGISTER_VALUE);
  volume_slider->setTickPosition(QSlider::TicksBothSides);
  connect(volume_slider, &QSlider::valueChanged, _q_sid_device, &QSIDDevice::setVolume);
  main_layout->addRow("Volume", volume_slider);

  return volume_widget;
}

}       // namespace
