/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_Q_SID_FILTER_PANEL_H_
#define _SID_GUI_Q_SID_FILTER_PANEL_H_

#include <QWidget>
#include <QCheckBox>

#include "../../core/sid/SIDVoice.h"
#include "../../core/sid/SIDFilter.h"
#include "../../core/sid/SIDDefs.h"

namespace sid_synth {

class QSIDFilterPanel: public QWidget {
  Q_OBJECT

public:
  explicit QSIDFilterPanel(QWidget* parent = nullptr);
  virtual ~QSIDFilterPanel() = default;

signals:
  void cutOffFrequencyChanged(SIDCutOffFrequency cut_off_frequency);
  void resonanceChanged(uint8_t resonance);
  void enableVoiceFilterChanged(SIDVoice::Id voice, bool is_enabled);
  void enableFilterChanged(const SIDFilter& filter, bool is_enabled);

private:
  QWidget* createEnableVoiceWidget();
  QCheckBox* createEnableVoiceCheckBox(const QString& label, SIDVoice::Id sid_voice);
  QWidget* createEnableFilterWidget();
  QCheckBox* createEnableFilterCheckBox(const QString& label, const SIDFilter& sid_filter);
};

}       // namespace
#endif  // _SID_GUI_Q_SID_FILTER_PANEL_H_
