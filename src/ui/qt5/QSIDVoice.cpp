/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "QSIDVoice.h"

namespace sid_synth {

QSIDVoice::QSIDVoice(SIDVoice& sid_voice, QObject* parent) :
    QObject(parent),
    _sid_voice(sid_voice) {
}

void QSIDVoice::enableShape(const SIDVoiceShape& shape, bool is_enabled) {
  _sid_voice.enableShape(shape, is_enabled);
}

void QSIDVoice::setFrequency(SIDFrequency frequency) {
  _sid_voice.setFrequency(frequency);
}

void QSIDVoice::enableGate(bool is_enabled) {
  _sid_voice.enableGate(is_enabled);
}

void QSIDVoice::setAttack(uint8_t attack) {
  _sid_voice.setAttack(attack);
}

void QSIDVoice::setDecay(uint8_t decay) {
  _sid_voice.setDecay(decay);
}

void QSIDVoice::setSustain(uint8_t sustain) {
  _sid_voice.setSustain(sustain);
}

void QSIDVoice::setRelease(uint8_t release) {
  _sid_voice.setRelease(release);
}

void QSIDVoice::setPulseWidth(uint16_t width) {
  _sid_voice.setPulseWidth(width);

}
void QSIDVoice::enableRing(bool is_enabled) {
  _sid_voice.enableRing(is_enabled);
}

void QSIDVoice::enableSync(bool is_enabled) {
  _sid_voice.enableSync(is_enabled);
}

void QSIDVoice::enableFilter(bool is_enabled) {
  _sid_voice.enableFilter(is_enabled);
}

}       // namespace
