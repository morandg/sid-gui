/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "QSIDDevice.h"

namespace sid_synth {

QSIDDevice::QSIDDevice(std::unique_ptr<ISIDDevice> sid_device, QWidget* parent) :
    QObject(parent),
    _sid_device(std::move(sid_device)),
    _voice1(new QSIDVoice(_sid_device->voice(SIDVoice::Voice1), this)),
    _voice2(new QSIDVoice(_sid_device->voice(SIDVoice::Voice2), this)),
    _voice3(new QSIDVoice(_sid_device->voice(SIDVoice::Voice3), this)) {
}

QSIDVoice* QSIDDevice::voice(SIDVoice::Id id) {
  switch(id) {
    case SIDVoice::Voice1:
      return _voice1;
    case SIDVoice::Voice2:
      return _voice2;
    case SIDVoice::Voice3:
    default:
      return _voice3;
  }
}

void QSIDDevice::open() {
  if(_sid_device) {
    emit deviceOpened(_sid_device->open() == 0);
  }
}

void QSIDDevice::setVolume(uint8_t volume) {
  if(_sid_device) {
    _sid_device->setVolume(volume);
  }
}

void QSIDDevice::setCutOffFrequency(SIDCutOffFrequency cut_off_frequency) {
  if(_sid_device) {
    _sid_device->setCutOffFrequency(cut_off_frequency);
  }
}

void QSIDDevice::setResonance(uint8_t resonance) {
  if(_sid_device) {
    _sid_device->setResonance(resonance);
  }
}

void QSIDDevice::enableFilter(SIDFilter::Filter filter, bool is_enabled) {
  if(_sid_device) {
    _sid_device->enableFilter(filter, is_enabled);
  }
}

}       // namespace
