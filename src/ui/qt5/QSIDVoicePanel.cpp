/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QGridLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QLabel>
#include <QCheckBox>

#include "QSIDVoicePanel.h"

namespace sid_synth {

QSIDVoicePanel::QSIDVoicePanel(QSIDVoice* sid_voice, QWidget* parent):
    QWidget(parent) {
  QGridLayout* main_layout = new QGridLayout(this);
  main_layout->addWidget(createVoiceShapeGroupBox(sid_voice), 0, 0, 1, 2);
  main_layout->addWidget(createADSRGroupBox(sid_voice), 0, 2, 1, 2);

  main_layout->addWidget(new QLabel("Frequency"), 1, 0);
  QSlider* slider_frequency = new QSlider(Qt::Horizontal, this);
  slider_frequency->setMinimum(0);
  slider_frequency->setMaximum(SID_MAX_FREQUENCY);
  slider_frequency->setTickPosition(QSlider::TicksBothSides);
  slider_frequency->setTickInterval(slider_frequency->maximum() / 10);
  connect(slider_frequency, &QSlider::valueChanged, sid_voice, &QSIDVoice::setFrequency);
  main_layout->addWidget(slider_frequency, 1, 1);

  QCheckBox* checkbox_sync = new QCheckBox("Sync", this);
  connect(checkbox_sync, &QCheckBox::clicked, sid_voice, &QSIDVoice::enableSync);
  main_layout->addWidget(checkbox_sync, 1, 2);
  
  QCheckBox* checkbox_ring = new QCheckBox("Ring", this);
  connect(checkbox_ring, &QCheckBox::clicked, sid_voice, &QSIDVoice::enableRing);
  main_layout->addWidget(checkbox_ring, 1, 3);

  QPushButton* gate_button = new QPushButton("Gate", this);
  gate_button->setCheckable(true);
  main_layout->addWidget(gate_button, 2, 0, 1, 4);
  connect(gate_button, &QAbstractButton::toggled, sid_voice, &QSIDVoice::enableGate);

  main_layout->setColumnStretch(1, 1);
  main_layout->setColumnStretch(3, 1);
}

QGroupBox* QSIDVoicePanel::createVoiceShapeGroupBox(QSIDVoice* sid_voice) {
  QGroupBox* groupbox_shapes = new QGroupBox("Shapes", this);
  QVBoxLayout* main_layout = new QVBoxLayout(groupbox_shapes);

  main_layout->addWidget(createShapesWidget(sid_voice));
  main_layout->addWidget(createPulseWidthWidget(sid_voice));

  return groupbox_shapes;
}

QGroupBox* QSIDVoicePanel::createADSRGroupBox(QSIDVoice* sid_voice) {
  QGridLayout* main_layout = new QGridLayout();
  QGroupBox* groupbox_adsr = new QGroupBox("Envelope", this);

  main_layout->addWidget(new QLabel("Attack", this), 0, 0);
  QSlider* spinbox_attack = createEnvelopeSlider();
  connect(spinbox_attack, &QSlider::valueChanged, sid_voice, &QSIDVoice::setAttack);
  main_layout->addWidget(spinbox_attack, 0, 1);

  main_layout->addWidget(new QLabel("Decay", this), 0, 2);
  QSlider* spinbox_decay = createEnvelopeSlider();
  connect(spinbox_decay, &QSlider::valueChanged, sid_voice, &QSIDVoice::setDecay);
  main_layout->addWidget(spinbox_decay, 0, 3);

  main_layout->addWidget(new QLabel("Sustain", this), 1, 0);
  QSlider* spinbox_sustain = createEnvelopeSlider();
  connect(spinbox_sustain, &QSlider::valueChanged, sid_voice, &QSIDVoice::setSustain);
  main_layout->addWidget(spinbox_sustain, 1, 1);

  main_layout->addWidget(new QLabel("Release", this), 1, 2);
  QSlider* spinbox_release = createEnvelopeSlider();
  connect(spinbox_release, &QSlider::valueChanged, sid_voice, &QSIDVoice::setRelease);
  main_layout->addWidget(spinbox_release, 1, 3);

  groupbox_adsr->setLayout(main_layout);

  return groupbox_adsr;
}

QSlider* QSIDVoicePanel::createEnvelopeSlider() {
  QSlider* envelope_slider = new QSlider(Qt::Horizontal, this);

  envelope_slider->setMinimum(0);
  envelope_slider->setMaximum(SID_MAX_4_BITS_REGISTER_VALUE);
  envelope_slider->setTickPosition(QSlider::TicksBothSides);

  return envelope_slider;
}

QWidget* QSIDVoicePanel::createShapesWidget(QSIDVoice* sid_voice) {
  QWidget* shapes_widget = new QWidget(this);
  QHBoxLayout* main_layout = new QHBoxLayout(shapes_widget);

  QCheckBox* checkbox_triangle = new QCheckBox("Triangle");
  connect(checkbox_triangle, &QCheckBox::clicked, this, [sid_voice](bool is_checked) {
    sid_voice->enableShape(SIDVoiceShape::Triangle, is_checked);
  });
  main_layout->addWidget(checkbox_triangle);

  QCheckBox* checkbox_pulse = new QCheckBox("Pulse");
  connect(checkbox_pulse, &QCheckBox::clicked, this, [sid_voice](bool is_checked) {
    sid_voice->enableShape(SIDVoiceShape::Pulse, is_checked);
  });
  main_layout->addWidget(checkbox_pulse);

  QCheckBox* checkbox_saw = new QCheckBox("Saw");
  connect(checkbox_saw, &QCheckBox::clicked, this, [sid_voice](bool is_checked) {
    sid_voice->enableShape(SIDVoiceShape::Saw, is_checked);
  });
  main_layout->addWidget(checkbox_saw);

  QCheckBox* checkbox_noise = new QCheckBox("Noise");
  connect(checkbox_noise, &QCheckBox::clicked, this, [sid_voice](bool is_checked) {
      emit sid_voice->enableShape(SIDVoiceShape::Noise, is_checked);
  });
  main_layout->addWidget(checkbox_noise);

  return shapes_widget;
}

QWidget* QSIDVoicePanel::createPulseWidthWidget(QSIDVoice* sid_voice) {
  QWidget* pulse_width_widget = new QWidget(this);
  QFormLayout* main_layout = new QFormLayout(pulse_width_widget);

  QSlider* slider_pulse_width = new QSlider(Qt::Horizontal, this);
  slider_pulse_width->setMinimum(0);
  slider_pulse_width->setMaximum(SID_MAX_PULSE_WIDTH);
  slider_pulse_width->setTickPosition(QSlider::TicksBothSides);
  slider_pulse_width->setTickInterval(slider_pulse_width->maximum() / 10);
  connect(slider_pulse_width, &QSlider::valueChanged, sid_voice, &QSIDVoice::setPulseWidth);
  main_layout->addRow("Pulse width", slider_pulse_width);

  return pulse_width_widget;
}

}       // namespace
