/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_Q_SID_DEVICE_H_
#define _SID_GUI_Q_SID_DEVICE_H_

#include <memory>

#include <QWidget>

#include "../../core/sid/ISIDDevice.h"
#include "QSIDVoice.h"

namespace sid_synth {

class QSIDDevice: public QObject {
  Q_OBJECT

public:
  explicit QSIDDevice(std::unique_ptr<ISIDDevice> sid_device, QWidget* parent = nullptr);
  virtual ~QSIDDevice() = default;

  QSIDVoice* voice(SIDVoice::Id id);

signals:
  void deviceOpened(bool is_success);

public slots:
  void open();
  void setVolume(uint8_t volume);
  void setCutOffFrequency(SIDCutOffFrequency cut_off_frequency);
  void setResonance(uint8_t resonance);
  void enableFilter(SIDFilter::Filter filter, bool is_enabled);

private:
  std::unique_ptr<ISIDDevice> _sid_device;
  QSIDVoice* _voice1;
  QSIDVoice* _voice2;
  QSIDVoice* _voice3;
};

}       // namespace
#endif  // _SID_GUI_Q_SID_DEVICE_H_
