/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_Q_SID_VOICE_H_
#define _SID_GUI_Q_SID_VOICE_H_

#include <QObject>

#include "../../core/sid/SIDVoice.h"

namespace sid_synth {

class QSIDVoice: public QObject {
  Q_OBJECT

public:
  explicit QSIDVoice(SIDVoice& sid_voice, QObject* parent = nullptr);
  virtual ~QSIDVoice() = default;

public slots:
  void enableShape(const SIDVoiceShape& shape, bool is_enabled);
  void setFrequency(SIDFrequency frequency);
  void enableGate(bool is_enabled);
  void setAttack(uint8_t attack);
  void setDecay(uint8_t decay);
  void setSustain(uint8_t sustain);
  void setRelease(uint8_t release);
  void setPulseWidth(uint16_t width);
  void enableRing(bool is_enabled);
  void enableSync(bool is_enabled);
  void enableFilter(bool is_enabled);

private:
  SIDVoice& _sid_voice;
};

}       // namespace
#endif  // _SID_GUI_Q_SID_DEVICE_H_
