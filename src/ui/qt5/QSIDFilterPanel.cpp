/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QFormLayout>
#include <QSlider>

#include "QSIDFilterPanel.h"

namespace sid_synth {

QSIDFilterPanel::QSIDFilterPanel(QWidget* parent):
    QWidget(parent) {
  QFormLayout* main_layout = new QFormLayout(this);

  main_layout->addRow(createEnableVoiceWidget());

  QSlider* cut_off_slider = new QSlider(Qt::Horizontal, this);
  cut_off_slider->setMinimum(0);
  cut_off_slider->setMaximum(SID_MAX_CUT_OFF_FREQUENCY);
  cut_off_slider->setTickPosition(QSlider::TicksBothSides);
  cut_off_slider->setTickInterval(cut_off_slider->maximum() / SID_MAX_4_BITS_REGISTER_VALUE);
  connect(cut_off_slider, &QSlider::valueChanged, this, [this](int value) {
    emit cutOffFrequencyChanged(value);
  });
  main_layout->addRow("Cutoff frequency", cut_off_slider);

  QSlider* resonance_slider = new QSlider(Qt::Horizontal, this);
  resonance_slider->setMinimum(0);
  resonance_slider->setMaximum(SID_MAX_4_BITS_REGISTER_VALUE);
  resonance_slider->setTickPosition(QSlider::TicksBothSides);
  connect(resonance_slider, &QSlider::valueChanged, this, [this](int value) {
      emit resonanceChanged(value);
  });
  main_layout->addRow("Resonance", resonance_slider);

  main_layout->addRow(createEnableFilterWidget());
}

QWidget* QSIDFilterPanel::createEnableVoiceWidget() {
  QWidget* enable_voice_widget = new QWidget(this);
  QHBoxLayout* main_layout = new QHBoxLayout(enable_voice_widget);

  main_layout->addWidget(createEnableVoiceCheckBox("Voice 1", SIDVoice::Voice1));
  main_layout->addWidget(createEnableVoiceCheckBox("Voice 2", SIDVoice::Voice2));
  main_layout->addWidget(createEnableVoiceCheckBox("Voice 3", SIDVoice::Voice3));

  return enable_voice_widget;
}

QCheckBox* QSIDFilterPanel::createEnableVoiceCheckBox(const QString& label, SIDVoice::Id sid_voice) {
  QCheckBox* enable_voice_checkbox = new QCheckBox(label, this);
  connect(enable_voice_checkbox, &QCheckBox::toggled, this, [this, sid_voice](bool is_checked) {
      emit enableVoiceFilterChanged(sid_voice, is_checked);
  });

  return enable_voice_checkbox;
}

QWidget* QSIDFilterPanel::createEnableFilterWidget() {
  QWidget* enable_filter_widget = new QWidget(this);
  QHBoxLayout* main_layout = new QHBoxLayout(enable_filter_widget);

  main_layout->addWidget(createEnableFilterCheckBox("Low pass", SIDFilter::LowPass));
  main_layout->addWidget(createEnableFilterCheckBox("Band pass", SIDFilter::BandPass));
  main_layout->addWidget(createEnableFilterCheckBox("High pass", SIDFilter::HighPass));
  main_layout->addWidget(createEnableFilterCheckBox("Voice 3 off", SIDFilter::Voice3Off));

  return enable_filter_widget;
}

QCheckBox* QSIDFilterPanel::createEnableFilterCheckBox(const QString& label, const SIDFilter& sid_filter) {
  QCheckBox* enable_filter_checkbox = new QCheckBox(label, this);
  connect(enable_filter_checkbox, &QCheckBox::toggled, this, [this, sid_filter](bool is_checked) {
      emit enableFilterChanged(sid_filter, is_checked);
  });

  return enable_filter_checkbox;
}

}       // namespace
