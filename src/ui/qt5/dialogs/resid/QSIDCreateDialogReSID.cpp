/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QDialogButtonBox>
#include <QFormLayout>

#include "QSIDCreateDialogReSID.h"

namespace sid_synth {

QSIDCreateDialogReSID::QSIDCreateDialogReSID(QWidget* parent) :
    QWidget(parent),
    _create_resid_form(new QCreateResidForm(this)),
    _create_dialog(new QSIDCreateDialog(_create_resid_form, this)) {
  connect(_create_dialog, &QSIDCreateDialog::createDeviceAccepted, this, [this]() {
    emit createReSIDDevice(_create_resid_form->selectedSIDModel());
  });
}

void QSIDCreateDialogReSID::show() {
  _create_dialog->show();
}

}       // namespace
