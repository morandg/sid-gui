/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QFormLayout>
#include <QLabel>

#include "QCreateResidForm.h"

namespace sid_synth {

QCreateResidForm::QCreateResidForm(QWidget* parent):
    QWidget(parent),
    _sid_model_combobox(new QComboBox(this)) {
  QFormLayout* main_layout = new QFormLayout(this);

  _sid_model_combobox->addItem("MOS6581", QVariant(static_cast<int>(SIDModel::MOS6581)));
  _sid_model_combobox->addItem("MOS8580", QVariant(static_cast<int>(SIDModel::MOS8580)));
  main_layout->addRow("Model", _sid_model_combobox);

  main_layout->addRow("Region", new QLabel("TODO -> PAL/NTSC", this));
  main_layout->addRow("Driver", new QLabel("TODO -> ALSA/JACKD", this));
}

SIDModel QCreateResidForm::selectedSIDModel() {
  QVariant selected_data = _sid_model_combobox->itemData(_sid_model_combobox->currentIndex());
  return static_cast<SIDModel>(selected_data.toInt());
}

}       // namespace
