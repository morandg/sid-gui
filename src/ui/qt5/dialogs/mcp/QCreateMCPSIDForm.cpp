/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QFormLayout>

#include "QCreateMCPSIDForm.h"

namespace sid_synth {

QCreeateMCPSidForm::QCreeateMCPSidForm(QWidget* parent):
    QWidget(parent),
    _line_edit_i2c_bus(new QLineEdit(this)),
    _spin_box_i2c_address(new QSpinBox(this)) {
  QFormLayout* main_layout = new QFormLayout(this);

  _line_edit_i2c_bus->setText("/dev/i2c-1");
  main_layout->addRow("I2C bus", _line_edit_i2c_bus);

  _spin_box_i2c_address->setMinimum(0x03);
  _spin_box_i2c_address->setMaximum(0x77);
  _spin_box_i2c_address->setValue(0x20);
  main_layout->addRow("I2C address", _spin_box_i2c_address);
}

QString QCreeateMCPSidForm::i2cBus() const {
  return _line_edit_i2c_bus->text();
}

uint8_t QCreeateMCPSidForm::i2cDeviceAddress() const {
  return static_cast<uint8_t>(_spin_box_i2c_address->value());
}

}       // namespace
