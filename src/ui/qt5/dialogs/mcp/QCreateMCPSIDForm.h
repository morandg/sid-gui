/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SID_GUI_Q_CREATE_MCP_SID_FORM_H_
#define _SID_GUI_Q_CREATE_MCP_SID_FORM_H_

#include <QLineEdit>
#include <QSpinBox>

namespace sid_synth {

class QCreeateMCPSidForm: public QWidget {
  Q_OBJECT

public:
  explicit QCreeateMCPSidForm(QWidget* parent = nullptr);
  virtual ~QCreeateMCPSidForm() = default;

  QString i2cBus() const;
  uint8_t i2cDeviceAddress() const;

private:
  QLineEdit* _line_edit_i2c_bus;
  QSpinBox* _spin_box_i2c_address;
};

}       // namespace
#endif  // _SID_GUI_Q_MCP_SID_CREATE_DIALOG_H_
