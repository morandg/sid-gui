/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QFormLayout>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QSpinBox>

#include "QSIDCreateDialogMCP.h"

namespace sid_synth {

QSIDCreateDialogMCP::QSIDCreateDialogMCP(QWidget* parent):
    QWidget(parent),
    _create_form(new QCreeateMCPSidForm(this)),
    _create_dialog(new QSIDCreateDialog(_create_form, this)) {
  connect(_create_dialog, &QSIDCreateDialog::createDeviceAccepted, this, [this](){
    emit createMCPSIDDevice(_create_form->i2cBus(), _create_form->i2cDeviceAddress());
  });
}

void QSIDCreateDialogMCP::show() {
  _create_dialog->show();
}

}       // namespace
