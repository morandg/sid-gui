/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QDialogButtonBox>
#include <QVBoxLayout>

#include "QSIDCreateDialog.h"

namespace sid_synth {

QSIDCreateDialog::QSIDCreateDialog(QWidget* central_widget, QWidget* parent):
    QDialog(parent) {
  QVBoxLayout* main_layout = new QVBoxLayout(this);
  QDialogButtonBox* accept_cancel_buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);

  main_layout->addWidget(central_widget),
  main_layout->addWidget(accept_cancel_buttons);
  setWindowModality(Qt::WindowModal);

  connect(accept_cancel_buttons, &QDialogButtonBox::accepted, this, [this]() {
      hide();
      emit createDeviceAccepted();
  });
  connect(accept_cancel_buttons, &QDialogButtonBox::rejected, this, [this]() {
      hide();
  });
}

}       // namespace
