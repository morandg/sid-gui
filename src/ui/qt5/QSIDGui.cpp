/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QMenuBar>
#include <QCoreApplication>
#include <QScrollArea>
#include <QLabel>
#include <QDebug>

#include "sid_synth_core_config.h"

#include "QSIDPanel.h"
#include "QSIDGui.h"

#ifdef HAS_I2C_LINUX_SUPPORT
#include "dialogs/mcp/QSIDCreateDialogMCP.h"
#endif

#ifdef HAS_RESID_SUPPORT
#include "dialogs/resid/QSIDCreateDialogReSID.h"
#endif

namespace sid_synth {

QSIDGui::QSIDGui(QWidget *parent) :
    QMainWindow(parent),
    _sid_tabs(new QTabWidget(this)) {
  setWindowTitle("SID synthesizer");
  buildMenu();

  _sid_tabs->setTabsClosable(true);
  connect(_sid_tabs, &QTabWidget::tabCloseRequested, this, [this](int index) {
    _sid_tabs->widget(index)->deleteLater();
    _sid_tabs->removeTab(index);
  });
  setCentralWidget(_sid_tabs);

  show();
}

void QSIDGui::buildMenu() {
  // FIXME: Menu broken if built after new QMenubar under Qt 5.16?!
#ifdef HAS_I2C_LINUX_SUPPORT
  QSIDCreateDialogMCP* create_dialog_mcp = new QSIDCreateDialogMCP(this);
#endif
#ifdef HAS_RESID_SUPPORT
  QSIDCreateDialogReSID* create_dialog_resid = new QSIDCreateDialogReSID(this);
#endif
  QMenuBar* menu_bar = new QMenuBar(this);
  setMenuBar(menu_bar);

  // -- File --
  QMenu* menu_file = menu_bar->addMenu("&File");

  QMenu* menu_add_sid_device = menu_file->addMenu("&Add SID device");

#ifdef HAS_I2C_LINUX_SUPPORT
  QAction* action_add_mcp23017_sid = new QAction("&MCP23017", this);
  menu_add_sid_device->addAction(action_add_mcp23017_sid);
  connect(action_add_mcp23017_sid, &QAction::triggered, create_dialog_mcp, &QSIDCreateDialogMCP::show);
  connect(create_dialog_mcp, &QSIDCreateDialogMCP::createMCPSIDDevice,
          this, [this](const QString& i2c_bus, quint8 i2c_address) {
              addSidPanel(
                      ISIDDevice::CreateMCP23017(i2c_bus.toStdString(), i2c_address),
                      QString("MCP23017 ") + i2c_bus + "@" + QString::number(i2c_address));
          });
#endif

#ifdef HAS_RESID_SUPPORT
  QAction* action_add_resid = new QAction("&reSID", this);
  menu_add_sid_device->addAction(action_add_resid);
  connect(action_add_resid, &QAction::triggered, create_dialog_resid, &QSIDCreateDialogReSID::show);
  connect(create_dialog_resid, &QSIDCreateDialogReSID::createReSIDDevice, this, [this](const SIDModel& model) {
    addSidPanel(ISIDDevice::CreateReSID(model), "reSID");
  });
#endif

  QAction* action_add_dummy_sid = new QAction("&Dummy", this);
  menu_add_sid_device->addAction(action_add_dummy_sid);
  connect(action_add_dummy_sid, &QAction::triggered, this, [this]() {
    addSidPanel(ISIDDevice::CreateDummy(), "Dummy");
  });

  QAction* action_quit = new QAction("&Quit", this);
  menu_file->addSeparator();
  menu_file->addAction(action_quit);
  connect(action_quit, &QAction::triggered, this, []() {
    QCoreApplication::quit();
  });
}

void QSIDGui::addSidPanel(std::unique_ptr<ISIDDevice> sid_device, const QString& label) {
  QScrollArea* scroll_area = new QScrollArea(_sid_tabs);
  scroll_area->setWidgetResizable(true);
  scroll_area->setBackgroundRole(QPalette::Light);
  scroll_area->setFrameShape(QFrame::NoFrame);

  if (sid_device) {
    scroll_area->setWidget(new QSIDPanel(std::move(sid_device), scroll_area));
  } else {
    scroll_area->setWidget(new QLabel("Could not instantiate SID device " + label));
  }
  _sid_tabs->setCurrentIndex(_sid_tabs->addTab(scroll_area, label));
}

}       // namespace
