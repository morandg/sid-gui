find_package(Qt5 COMPONENTS Widgets Gui)
if(${Qt5_FOUND} AND ${QT_GUI})
    add_subdirectory(qt5)
endif()

if(${DEARIMGUI_GUI})
    add_subdirectory(imgui)
endif()