find_package(glfw3)

set(OpenGL_GL_PREFERENCE GLVND)
find_package(OpenGL)

if(${glfw3_FOUND} AND ${OpenGL_FOUND})
    set(IMGUI_APP_NAME ${PROJECT_NAME}-imgui)
    set(IMGUI_APP_SOURCES
        ImGuiSidGui.cpp
        main.cpp)
    include_directories(
        ../../../lib/imgui-1.84.2)

    add_executable(
        ${IMGUI_APP_NAME}
        ${IMGUI_APP_SOURCES})

    target_link_libraries(
        ${IMGUI_APP_NAME}
        ${APP_CORE_LIB_NAME}
        ${APP_CORE_LIB_EXTRA_LIB}
        ${IMGUI_LIB_NAME}
        glfw
        OpenGL::OpenGL
        ${CMAKE_DL_LIBS})

    install(TARGETS ${IMGUI_APP_NAME} DESTINATION bin/)
endif()