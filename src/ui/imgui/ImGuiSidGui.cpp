/**
 * SID synthesizer
 *
 * Copyright (C) 2021 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <imgui-1.84.2/imgui.h>

#include "ImGuiSidGui.h"

namespace sid_synth {

bool ImGuiSidGui::draw() {
  bool is_running = true;

  is_running = drawMenu();

  if(show_imgui_about) {
    ImGui::ShowAboutWindow(&show_imgui_about);
  }
  if(show_imgui_metrics) {
    ImGui::ShowMetricsWindow(&show_imgui_metrics);
  }
  if(show_imgui_demo) {
    ImGui::ShowDemoWindow(&show_imgui_demo);
  }

  return is_running;
}

bool ImGuiSidGui::drawMenu() {
  bool is_running = true;

  if(ImGui::BeginMainMenuBar()) {
    if(ImGui::BeginMenu("File")) {
      if(ImGui::BeginMenu("Add device")) {
        if(ImGui::MenuItem("Dummy")) {
          addSideDevice(ISIDDevice::CreateDummy());
        }
        ImGui::EndMenu();
      }
      if(ImGui::MenuItem("Exit")) {
        is_running = false;
      }
      ImGui::EndMenu();
    }

    if(ImGui::BeginMenu("Options")) {
      if(ImGui::BeginMenu("Theme")) {
        if(ImGui::MenuItem("Dark")) {
          ImGui::StyleColorsDark();
        }
        if(ImGui::MenuItem("Classic")) {
          ImGui::StyleColorsClassic();
        }
        if(ImGui::MenuItem("Light")) {
          ImGui::StyleColorsLight();
        }
        ImGui::EndMenu();
      }
      ImGui::EndMenu();
    }

    if(ImGui::BeginMenu("Debugging")) {
      if(ImGui::MenuItem("Dear ImGui metrics")) {
        show_imgui_metrics = true;
      }
      if(ImGui::MenuItem("Dear ImGui Demos")) {
        show_imgui_demo = true;
      }
      ImGui::EndMenu();
    }

    if(ImGui::BeginMenu("Help")) {
      if(ImGui::MenuItem("About Dear ImGui")) {
        show_imgui_about = true;
      }
      ImGui::EndMenu();
    }

    ImGui::EndMainMenuBar();
  }

  return is_running;
}

void ImGuiSidGui::addSideDevice(std::unique_ptr<ISIDDevice> sid_device) {
  if(sid_device && sid_device->open() == 0) {
    _sid_devices.push_back(std::move(sid_device));
  }
}

}       // namespace
