cmake_minimum_required(VERSION 3.7)
project(
    SID-synth
    VERSION 0.0.0)

if("${CMAKE_BUILD_TYPE}" STREQUAL "")
  set(CMAKE_BUILD_TYPE Release)
endif()

option(RUN_TESTS "Run the tests" ON)
option(LINUX_I2C_SUPPORT "Add Linux i2c support for MCP23017" ON)
option(RESID_SUPPORT "Add reSID software emulation support" ON)
option(QT_GUI "Compile Qt GUI" ON)
option(DEARIMGUI_GUI "Compile Dear ImGui GUI" ON)

set(APP_CORE_LIB_NAME ${PROJECT_NAME})
set(RESID_LIB_NAME reSID)
set(IMGUI_LIB_NAME imgui)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror")

find_package(PkgConfig REQUIRED)

add_subdirectory(lib)
add_subdirectory(src)

pkg_search_module(CPPUTEST cpputest>=3.8)
if(${CPPUTEST_FOUND})
  message(STATUS "Found CppUTest version ${CPPUTEST_VERSION}, building tests")
  add_subdirectory(tests)
endif(${CPPUTEST_FOUND})
